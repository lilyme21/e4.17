///////////////////////////////////////////////////////////////
// Lily Mendoza
// CS 49J
// 9/12/19
// This program will read two user inputs as two military times.
// Then it will calculate and print out the hours and minutes
// between the two times.
///////////////////////////////////////////////////////////////

import java.util.Scanner;       // importing util package to use Scanner

public class E4_17 {
    public static void main(String[] args) {
        // initializing variables for:
        int btwnTime = 0;       // the time in between the users times
        int hours = 0;          // the hours in between the users times
        int minutes = 0;        // the minutes in between the users times
        Scanner in = new Scanner(System.in);
        // prompting user for a time in military format
        System.out.println("Please enter a time in military format: ");
        // getting a string input from user for the first time
        int firstTime = in.nextInt();
        // prompting user for a second time in military format
        System.out.println("Please enter a second time in military format: ");
        // getting a string input from user for the second time
        int secondTime = in.nextInt();
        // creating an if statement to find which time is lesser and calculate btwnTime
        if (firstTime < secondTime) {
            btwnTime = secondTime - firstTime;
        }
        else {
            btwnTime = 2360 - firstTime + secondTime;
        }
        hours = btwnTime/100;   // calculating hours
        minutes = btwnTime%100; // calculating minutes
        // creating an if statement to check and correct minute error
        if (minutes >= 60) {
            // converting extra minutes to hours
            minutes -= 60;      // subtracting 60 minutes from minutes
            hours += 1;         // adding one hour to hours
        }
        // printing out the result
        System.out.println(hours + " hours and " + minutes + " minutes");
    }
}
